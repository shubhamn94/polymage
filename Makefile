# Auto-detect python3 version
PYTHON3_VERSION_FULL := $(wordlist 2,4,$(subst ., ,$(shell python3 --version 2>&1)))
PYTHON3_VERSION_MAJOR := $(word 1,${PYTHON3_VERSION_FULL})
PYTHON3_VERSION_MINOR := $(word 2,${PYTHON3_VERSION_FULL})
PYTHON3_VERSION := $(PYTHON3_VERSION_MAJOR).$(PYTHON3_VERSION_MINOR)

INCLUDES=-I/usr/include/python$(PYTHON3_VERSION)m/ 
CPPFLAGS=-shared -lpython$(PYTHON3_VERSION)m -fPIC -std=c++11 -Wall
LDFLAGS=-lboost_system 

all: sandbox/dpfusion.so

sandbox/dpfusion.so: sandbox/dpfusion/dpfusion.cpp
	g++ -D DEBUG=0 $(INCLUDES) $(CPPFLAGS) -O3 -o $@ $< $(LDFLAGS)

debug1:	sandbox/dpfusion/dpfusion.cpp
	g++ -D DEBUG=1 $(INCLUDES) $(CPPFLAGS) -O3 -o sandbox/dpfusion.so $< $(LDFLAGS)

debug2:	sandbox/dpfusion/dpfusion.cpp
	g++ -D DEBUG=2 $(INCLUDES) $(CPPFLAGS) -O3 -o sandbox/dpfusion.so $< $(LDFLAGS)
	
debug3:	sandbox/dpfusion/dpfusion.cpp
	g++ -D DEBUG=3 $(INCLUDES) $(CPPFLAGS) -O3 -o sandbox/dpfusion.so $< $(LDFLAGS)

gdbdebug: sandbox/dpfusion/dpfusion.cpp
	g++ -g -D DEBUG=0 -D GDB_DEBUG $(INCLUDES) $(CPPFLAGS) -O0 -o sandbox/dpfusion.so $< $(LDFLAGS)

clean:
	rm -f sandbox/dpfusion.so
