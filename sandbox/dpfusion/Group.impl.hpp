//#include "Group.h"
//Find if removing a child will make graph disconnected
//If true, then fill the vector components with 2 disconnected components
bool OptGroup::isDisconnected2 (OptGroup* toRemove, std::vector< std::vector <OptGroup*> >& components)
{
    //Using Kosaraju’s DFS to find if removing the child would make
    //the group disconnected
    stack<OptGroup*> s;
    unordered_map<uint128_t, bool, uint128Hasher> visited;
    OptGroup* start = NULL;
    bool _isDisconnected = false;
    
    for (auto it = children.begin(); it != children.end (); it++)
    {
        if ((*it)->hashID() != toRemove->hashID())
            start = *it;
        
        visited[(*it)->hashID ()] = false;
    }
    
    s.push (start);
    
    while (s.size () != 0)
    {
        OptGroup* g = s.top ();
        s.pop ();
        
        visited[g->hashID()] = true;
        
        for (auto it = g->nextGroups ().begin (); 
              it != g->nextGroups().end (); it++)
        {
            if (((*it)->hashID() & hashID ()) == (*it)->hashID () &&
                (*it)->hashID () != toRemove->hashID () && 
                visited[(*it)->hashID()] == false)
            {
                s.push ((OptGroup*)*it);
            }
        }
    }
    
    unordered_map<uint128_t, bool, uint128Hasher> reverseVisited;
    
    for (auto it = children.begin(); it != children.end (); it++)
    {
        reverseVisited[(*it)->hashID ()] = false;
    }
    
    s.push (start);
    
    while (s.size () != 0)
    {
        OptGroup* g = s.top ();
        s.pop ();
        
        reverseVisited[g->hashID ()] = true;
        
        for (auto it = g->prevGroups ().begin (); 
              it != g->prevGroups().end (); it++)
        {
            if (((*it)->hashID() & hashID ()) == (*it)->hashID () &&
                (*it)->hashID () != toRemove->hashID () &&
                reverseVisited[(*it)->hashID()] == false)
            {
                s.push ((OptGroup*)*it);
            }
        }
    }
    
    for (auto it = children.begin (); it != children.end (); it++)
    {
        if ((*it)->hashID() != toRemove->hashID ())
        {            
            if (visited[(*it)->hashID ()] == false &&
                reverseVisited[(*it)->hashID()] == false)
            {
                _isDisconnected = true;
                components[1].push_back (*it);
            }
            else
            {
                components[0].push_back (*it);
            }
        }   
    }
    
    return _isDisconnected;
}
    
bool OptGroup::isDisconnected (OptGroup* toRemove, vector<unordered_set <OptGroup*> >& components)
{
    //If either all of toRemove's previous or next groups are not in this group
    //Then graph will not become disconnected.
    if (allNextGroupsAreNotChild (toRemove))
        return false;

    vector<OptGroup*> source_vertices;
    unordered_map<uint128_t, bool, uint128Hasher> visited;
      //vector of vertices reachable from one vertex using nextGroups
    unordered_map<OptGroup*, unordered_set <OptGroup*> > reachable_vertices;
    
    for (auto it = children.begin(); it != children.end (); it++)
    {
        stack<OptGroup*> s;
        
        if ((*it)->hashID() == toRemove->hashID())
            continue;
        
        unordered_set <OptGroup*> reachable_set;
        
        s.push ((OptGroup*)*it);
        
        while (s.size () != 0)
        {
            OptGroup* g = s.top ();
            s.pop ();
            
            for (auto it2 = g->nextGroups ().begin (); 
                it2 != g->nextGroups().end (); it2++)
            {
                if (((*it2)->hashID() & hashID ()) == (*it2)->hashID () &&
                    (*it2)->hashID () != toRemove->hashID ())
                {
                    reachable_set.insert ((OptGroup*)*it2);
                    s.push ((OptGroup*)*it2);
                }
            }
        }
        
        reachable_vertices [*it] = reachable_set;
    }
    
    for (auto it = reachable_vertices.begin(); 
          it != reachable_vertices.end(); it++)
    {
        PRINT_DEBUG_L1 (std::cout<< std::bitset<41>((uint64_t)it->first->hashID ())<<std::endl;);
        
        for (auto it2 = it->second.begin(); it2 != it->second.end(); it2++)
        {
            PRINT_DEBUG_L1 (std::cout<< "    " << std::bitset <41>((uint64_t) (*it2)->hashID ())<< std::endl;);
        }
    }
    
    unordered_map <OptGroup*, int> group_to_set_index;
    
    for (auto it = reachable_vertices.begin(); 
          it != reachable_vertices.end(); it++)
    {
        unordered_set <OptGroup*>* foundInSet = NULL;
        
        for (auto it2 = components.begin (); it2 != components.end(); it2++)
        {
            if ((*it2).find (it->first) != (*it2).end ())
            {
                foundInSet = &(*it2);
                break;
            }
        }
        
        if (foundInSet == NULL)
        {
            for (auto it2 = it->second.begin(); it2 != it->second.end (); it2++)
            {
                if (group_to_set_index.find ((OptGroup*)*it2) != 
                    group_to_set_index.end ())
                {
                    foundInSet = &components [group_to_set_index [(OptGroup*)*it2]];
                    break;
                }
            }
        }
        
        if (foundInSet != NULL)
        {
            for (auto it2 = it->second.begin(); it2 != it->second.end(); it2++)
            {
                foundInSet->insert ((OptGroup*)*it2);
            }
            
            foundInSet->insert (it->first);
        }
        else
        {                
            unordered_set<OptGroup*> s = it->second;
            s.insert (it->first);
            components.push_back (s);
            group_to_set_index [it->first] = components.size () - 1;
        }
    }
    
    if (components.size () == 1)
        return false;
        
    return true;
}
    
bool OptGroup::createsCycle (OptGroup* toRemove, std::vector< std::vector <OptGroup*> >& components)
{
    //If toRemove's previous group's (in the parent) can reach toRemove's next group's (in the parent)
    //by not passing through toRemove, then there will be cycle after removing toRemove
    
    vector <OptGroup*> prev_children;
    vector <OptGroup*> next_children;
    bool is_createsCycle = false;
    
    for (auto it = toRemove->prevGroups ().begin ();
          it != toRemove->prevGroups ().begin (); it++)
    {
        if ((hashID()& (*it)->hashID()) == (*it)->hashID())
            prev_children.push_back ((OptGroup*)*it);
    }
    
    for (auto it = toRemove->nextGroups ().begin ();
          it != toRemove->nextGroups ().begin (); it++)
    {
        if ((hashID()& (*it)->hashID()) == (*it)->hashID())
            next_children.push_back ((OptGroup*)*it);
    }
    
    vector<OptGroup*> source_vertices;
    unordered_map<uint128_t, bool, uint128Hasher> visited;
    stack<OptGroup*> s;
    
    for (auto it = prev_children.begin(); it != prev_children.end (); it++)
    {
        s.push (*it);
    }
    
    while (s.size () != 0)
    {
        OptGroup* g = s.top ();
        s.pop ();
        
        if (toRemove->nextGroups ().find (g) != 
            toRemove->nextGroups ().end())
        {
            is_createsCycle = true;
            break;
        }
        
        for (auto it = g->nextGroups ().begin ();
              it != g->nextGroups().end (); it++)
        {
            if (((*it)->hashID() & hashID ()) == (*it)->hashID () &&
                (*it)->hashID () != toRemove->hashID ())
            {
                s.push ((OptGroup*)*it);
            }
        }
    }

    if (!is_createsCycle)
        return false;
    
    while (s.size () != 0)
        s.pop ();
    
    for (auto it = next_children.begin(); it != next_children.end (); it++)
    {
        s.push (*it);
    }
    
    while (s.size () != 0)
    {
        OptGroup* g = s.top ();
        s.pop ();
        
        visited[g->hashID()] = true;
        
        for (auto it = g->nextGroups ().begin (); 
              it != g->nextGroups().end (); it++)
        {
            if (((*it)->hashID() & hashID ()) == (*it)->hashID () &&
                (*it)->hashID () != toRemove->hashID ())
            {
                s.push ((OptGroup*)*it);
            }
        }
    }
    
    for (auto it = children.begin (); it != children.end (); it++)
    {
        if ((*it)->hashID() != toRemove->hashID ())
        {            
            if (visited[(*it)->hashID ()] == false)
            {
                components[1].push_back (*it);
            }
            else
            {
                components[0].push_back (*it);
            }
        }   
    }
    
    if (components[1].size () == 0)
        return false;
        
    return true;
}
    
bool OptGroup::minParentEdgeToChild (OptGroup* toRemove, std::vector< unordered_set <OptGroup*> >& components, 
                    OptGroup* minParentGroup)
{
    if (!minParentGroup)
    {
        printf ("minParentEdgeToChild: minParentGroup is NULL\n");
        assert (false);
    }
        
    //If a child of min parent group has an edge to a child of this group
    //then break this group as it can create cycles
    
    bool toReturn = false;
    vector<OptGroup*> childrenToDisconnect;
    
    for (auto it = minParentGroup->childGroups ().begin(); 
          it != minParentGroup->childGroups ().end(); it++)
    {
        for (auto it2 = (*it)->nextGroups ().begin(); 
              it2 != (*it)->nextGroups ().end(); it2++)
        {
            if (((*it2)->hashID () & hashID ()) == (*it2)->hashID ())
            {
                childrenToDisconnect.push_back ((OptGroup*)(*it2));
                toReturn = true;
            }
        }
    }
    
    
    PRINT_DEBUG_BLOCK_L3 {
        std::cout<< "childrenToDisconnect "<< childrenToDisconnect.size () << std::endl;
        for (auto it = childrenToDisconnect.begin(); it != childrenToDisconnect.end (); it++)
        {
            std::cout<< std::bitset<41>((uint64_t)(*it)->hashID ())<< std::endl;
        }
    }
    
    if (!toReturn)
        return toReturn;
    
    vector<OptGroup*> source_vertices;
    unordered_map<uint128_t, bool, uint128Hasher> visited;
    unordered_map<OptGroup*, unordered_set <OptGroup*> > reachable_vertices;
    
    components.clear ();
    
    for (auto it = children.begin(); it != children.end (); it++)
    {
        visited [(*it)->hashID ()] = false;
    }
    
    for (auto it = childrenToDisconnect.begin(); 
          it != childrenToDisconnect.end (); it++)
    {
        stack<OptGroup*> s;
        unordered_set <OptGroup*> reachable_set;
        s.push (*it);
        
        while (s.size () != 0)
        {
            OptGroup* g = s.top ();
            s.pop ();
            visited[g->hashID()] = true;
            
            for (auto it2 = g->nextGroups ().begin ();
                it2 != g->nextGroups().end (); it2++)
            {
                if (((*it2)->hashID() & hashID ()) == (*it2)->hashID () &&
                    (*it2)->hashID () != toRemove->hashID ())
                {
                    reachable_set.insert ((OptGroup*)*it2);
                    s.push ((OptGroup*)*it2);
                }
            }
        }
        
        reachable_vertices [*it] = reachable_set;
    }

    components.push_back (unordered_set <OptGroup*> ());
    
    for (auto it = children.begin (); it != children.end (); it++)
    {
        if (visited[(*it)->hashID ()] == false)
        {            
            components[0].insert (*it);
        }   
    }
    
    if (components [0].size () == 0)
        components.clear ();
    
    PRINT_DEBUG_BLOCK_L1 
    {
        std::cout<< "minParentEdgeToChild reachable_vertices: " << std::endl;
        for (auto it = reachable_vertices.begin(); 
            it != reachable_vertices.end(); it++)
        {
            std::cout<< std::bitset<41>((uint64_t)it->first->hashID ())<<std::endl;
            
            for (auto it2 = it->second.begin(); it2 != it->second.end(); it2++)
            {
                std::cout<< "    " << std::bitset <41>((uint64_t) (*it2)->hashID ())<< std::endl;
            }
        }
        
        std::cout<< "components EARLIER 0" << std::endl;
        for (auto it = components.begin(); it != components.end(); it++)
        {
            std::cout<< "component " << (it)-components.begin() << std::endl;
            for (auto it2 = (*it).begin (); it2 != (*it).end(); it2++)
            {
                std::cout<< "   " << std::bitset <41>((uint64_t)(*it2)->hashID()) << std::endl;
            }
        }
    }
    
    for (auto it = reachable_vertices.begin(); 
          it != reachable_vertices.end(); it++)
    {
        unordered_set <OptGroup*>* foundInSet = NULL;
        
        for (auto it2 = components.begin (); it2 != components.end(); it2++)
        {
            if ((*it2).find (it->first) != (*it2).end ())
            {
                foundInSet = &(*it2);
                break;
            }
        }
        
        if (foundInSet != NULL)
        {
            for (auto it2 = it->second.begin(); it2 != it->second.end(); it2++)
            {
                foundInSet->insert ((OptGroup*)*it2);
            }
            
            foundInSet->insert (it->first);
        }
        else
        {
            components.push_back (it->second);
            (*(components.end () - 1)).insert (it->first);
        }
    }
    
    PRINT_DEBUG_BLOCK_L1
    { 
        std::cout<< "components" << std::endl;
        for (auto it = components.begin(); it != components.end(); it++)
        {
            std::cout<< "component " << (it)-components.begin() << std::endl;
            for (auto it2 = (*it).begin (); it2 != (*it).end(); it2++)
            {
                std::cout<< "   " << std::bitset <41>((uint64_t)(*it2)->hashID()) << std::endl;
            }
        }
    }

    if (components.size () == 1)
        return false;
        
    return true;
}
    
void OptGroup::removeChild (uint128_t _hash_id, OptGroup* minParentGroup)
{
    vector <unordered_set <OptGroup*> > components;
    std::vector <OptGroup*> childrenToRemove;
    uint128_t prev_hash_id = hashID();

    if (this->hashID() == 0 || _hash_id == 0)
        return;
    
    OptGroup* toRemove = OptGroup::optHashIDToGroup[_hash_id];
    childrenToRemove.push_back (toRemove);

    //TODO: create functions for setting/getting values in parentOptGroupToHashID map and optGroupToHashID map
    //Also add assertions in it that the group being set/get is equal to the hash id key.
    
    if (_hash_id != hashID () &&
        (isDisconnected (childrenToRemove[0], 
        components) || minParentEdgeToChild (childrenToRemove[0], 
        components, minParentGroup)))
    {
        if (components.size() > 1)
        {
            auto components_it = components.begin ();
            
            components_it++; //the first one is current group
            while (components_it != components.end ())
            {
                unordered_set <OptGroup*>& connected_component = *components_it;
                uint128_t newHashID = (*connected_component.begin())->hashID();
                
                for (auto it = connected_component.begin (); it != connected_component.end ();
                    it++)
                {
                    newHashID = newHashID | (*it)->hashID ();
                    children.erase (OptGroup::optHashIDToGroup[(*it)->hashID ()]);
                    childrenToRemove.push_back (*it);
                }
            
                OptGroup* newGroup;
                
                if (!inParentOptGroupsToHashID (newHashID))
                {
                    newGroup = OptGroup::createParentOptGroup (newHashID);
                    vector <Group*> toRemove;
                
                    //Find next and prev groups for new group
                    
                    for (auto it2 = connected_component.begin (); 
                          it2 != connected_component.end (); it2++)
                    {
                        for (auto it3 = (*it2)->nextGroups ().begin (); 
                              it3 != (*it2)->nextGroups ().end (); it3++)
                        {
                            for (auto it4 = (*it3)->parentGroups ().begin ();
                                  it4 != (*it3)->parentGroups ().end ();
                                  it4++)
                            {
                                if ((*it4)->hashID () != newGroup->hashID ())
                                {
                                    newGroup->nextGroups ().insert (*it4);
                                    (*it4)->prevGroups (). insert (newGroup);
                                }
                            }
                        }
                    }
                    
                    for (auto it2 = connected_component.begin (); 
                          it2 != connected_component.end (); it2++)
                    {
                        for (auto it3 = (*it2)->prevGroups ().begin (); 
                              it3 != (*it2)->prevGroups ().end (); it3++)
                        {
                            for (auto it4 = (*it3)->parentGroups ().begin ();
                                  it4 != (*it3)->parentGroups ().end ();
                                  it4++)
                            {
                                if ((*it4)->hashID () != newGroup->hashID ())
                                {
                                    newGroup->prevGroups ().insert (*it4);
                                    (*it4)->nextGroups (). insert (newGroup);
                                }
                            }
                        }
                    }
                }
                else
                {
                    newGroup = parentOptGroupsToHashID[newHashID];
                }
                
                components_it++;
            }
        }
    }
    
    for (auto const &childToRemove : childrenToRemove)
    {
        vector<OptGroup*> prevGroupsToRemove, nextGroupsToRemove;
        
        (childToRemove)->parentGroups().erase (this);
        
        children.erase (childToRemove);
        
        //Remove those links from prevGroups and nextGroups to this group
        //which are due to children in childrenToRemove
        
        for (auto it2 = prevGroups().begin(); it2 != prevGroups().end(); it2++)
        {
            OptGroup* prevGrp = (OptGroup*)(*it2);
            
            for (auto it3 = childToRemove->prevGroups().begin (); 
                  it3 != childToRemove->prevGroups().end(); it3++)
            {
                if ((*it3)->parentGroups().find (prevGrp) != (*it3)->parentGroups().end())
                {
                    bool canRemove = true;
                    for (auto it4 = children.begin(); it4 != children.end();
                        it4++)
                    {
                        for (auto it5 = (*it4)->prevGroups().begin(); 
                              it5 != (*it4)->prevGroups().end(); it5++)
                        {
                            //Remove parent group only if it is the parent of only
                            //prev group of only one child
                            if ((*it5)->parentGroups().find (prevGrp) != (*it5)->parentGroups().end())
                            {
                                canRemove = false;
                                break;
                            }
                        }
                    }
                    
                    if (canRemove)
                        prevGroupsToRemove.push_back (prevGrp);
                }
            }
        }
        
        for (auto it2 = nextGroups().begin(); it2 != nextGroups().end(); it2++)
        {
            OptGroup* nextGrp = (OptGroup*)(*it2);
            
            for (auto it3 = childToRemove->nextGroups().begin (); 
                  it3 != childToRemove->nextGroups().end(); it3++)
            {
                if ((*it3)->parentGroups().find (nextGrp) != 
                    (*it3)->parentGroups().end())
                {
                    bool canRemove = true;
                    for (auto it4 = children.begin(); it4 != children.end();
                        it4++)
                    {
                        for (auto it5 = (*it4)->nextGroups().begin(); 
                              it5 != (*it4)->nextGroups().end(); it5++)
                        {
                            //Remove parent group only if it is the parent of only
                            //prev group of only one child
                            if ((*it5)->parentGroups().find (nextGrp) != (*it5)->parentGroups().end())
                            {
                                canRemove = false;
                                break;
                            }
                        }
                    }
                    
                    if (canRemove)
                        nextGroupsToRemove.push_back (nextGrp);
                }
            }
        }
        
        for (auto it2 = prevGroupsToRemove.begin();
              it2 != prevGroupsToRemove.end(); it2++)
        {
            prevGroups().erase (*it2);
            (*it2)->nextGroups().erase (this);
        }
        
        for (auto it2 = nextGroupsToRemove.begin();
              it2 != nextGroupsToRemove.end(); it2++)
        {
            nextGroups().erase (*it2);
            (*it2)->prevGroups().erase (this);
        }
    }
    
    //Clear prevGroups and nextGroups
    
    prevGroups().clear ();
    nextGroups().clear ();
    
    //Update prevGroup and nextGroup with updated children set
    for (auto const &child : children)
    {
        //Add parentGroups of prevGroups of child as prevGroups
        for (auto const &child_prev : child->prevGroups ())
        {
            if (children.find ((OptGroup*)child_prev) == children.end ())
                prevGroups().insert (child_prev->parentGroups().begin(),
                                      child_prev->parentGroups().end());
        }
        
        //Add parentGroups of nextGroups of child as nextGroups
        for (auto const &child_next : child->nextGroups ())
        {
            if (children.find ((OptGroup*)child_next) == children.end ())
                nextGroups().insert (child_next->parentGroups().begin(),
                                      child_next->parentGroups().end());
        }
    }
    
    //Update all sets where this group is contained because OptGroup
    //is hashed on the base of hash id. Changing hash id of group without 
    //updating there could (or will?) produce error
    for (auto it = children.begin(); it != children.end(); it++)
    {
        //Remove from parentGroup of children
        (*it)->parentGroups().erase (this);
    }
    
    for (auto it = nextGroups().begin(); it != nextGroups().end(); it++)
    {
        //Remove from prevGroup of next groups
        (*it)->prevGroups().erase (this);
    }
    
    for (auto it = prevGroups().begin(); it != prevGroups().end(); it++)
    {
        //Remove from nextGroup of prev groups
        (*it)->nextGroups().erase (this);
    }
    
    //Update Hash ID now because set of parents use hash id as its hash id
    for (auto it = childrenToRemove.begin (); it != childrenToRemove.end ();
          it++)
    {
        hash_id &= ~(*it)->hashID ();
    }
    
    if (hash_id != 0)
    {
        for (auto it = children.begin(); it != children.end(); it++)
        {
            //Insert to parentGroup of children
            (*it)->parentGroups().insert (this);
        }
        
        for (auto it = nextGroups().begin(); it != nextGroups().end(); it++)
        {
            //Insert to prevGroup of next groups
            (*it)->prevGroups().insert (this);
        }
        
        for (auto it = prevGroups().begin(); it != prevGroups().end(); it++)
        {
            //Insert to nextGroup of prev groups
            (*it)->nextGroups().insert (this);
        }
    }
    
    if (hash_id != 0)
        parentOptGroupsToHashID [hash_id] = this;
    parentOptGroupsToHashID.erase (prev_hash_id);
}
    
void OptGroup::setChildrenFromHashID ()
{        
    for (auto it = optHashIDToGroup.begin (); it != optHashIDToGroup.end (); ++it)
    {
        if (it->second != NULL && it->second != this && (hash_id & it->first) == it->first)
        {
            children.insert (it->second);
            it->second->parentGroups ().insert (this);
        }
    }
}

OptGroup* OptGroup::createOptGroup (uint128_t _hash_id)
{
    OptGroup* _opt_group = optHashIDToGroup[_hash_id];
    
    if (_opt_group == NULL)
    {
        _opt_group = new OptGroup (_hash_id);
        optHashIDToGroup[_hash_id] = _opt_group;
        vectorOptGroups.push_back (_opt_group);
    }
    
    return (OptGroup*)_opt_group;
}

bool OptGroup::inParentOptGroupsToHashID (uint128_t _hash_id)
{
    return parentOptGroupsToHashID.find(_hash_id) != 
        parentOptGroupsToHashID.end() && parentOptGroupsToHashID [_hash_id] != NULL;
}

OptGroup* OptGroup::createParentOptGroup (uint128_t _hash_id)
{
    OptGroup* _opt_group = parentOptGroupsToHashID[_hash_id];
    
    if (_opt_group == NULL)
    {
        _opt_group = new OptGroup (_hash_id);
        parentOptGroupsToHashID[_hash_id] = _opt_group;
        _opt_group->setChildrenFromHashID ();
    }
    
    return _opt_group;
}

vector<uint128_t>* OptGroup::nextGroupsHashID (uint128_t hash_id)
{
    Group* g;
    
    g = optHashIDToGroup[hash_id];

    if (g != NULL)
        return new vector<uint128_t> (g->nextGroupsHashID()->begin(), 
                                        g->nextGroupsHashID()->end());
                    
    vector<uint128_t>* next_groups;
    next_groups = new vector<uint128_t> ();
    set <uint128_t, uint128Comparer> _set;
        
    for (auto it = vectorOptGroups.begin (); it != vectorOptGroups.end (); ++it)
    {
        if ((hash_id & (*it)->hashID()) == (*it)->hashID())
        {
            vector<uint128_t>* a = ((Group*)(*it))->nextGroupsHashID();

            for (auto it2 = a->begin(); it2 != a->end(); ++it2)
            {
                if (((*it2) & hash_id) != (*it2))
                    next_groups->push_back (*it2);
            }
        }
    }
    
    for (auto it = _set.begin(); it != _set.end(); it++)
        next_groups->push_back (*it);
        
    return next_groups;
}

vector<uint128_t>* OptGroup::prevGroupsHashID (uint128_t hash_id)
{
    Group* g;
    
    g = optHashIDToGroup[hash_id];

    if (g != NULL)
        return new vector<uint128_t> (g->prevGroupsHashID()->begin(), 
                                        g->prevGroupsHashID()->end());
                    
    vector<uint128_t>* prev_groups;
    prev_groups = new vector<uint128_t> ();
    
    for (auto it = optHashIDToGroup.begin (); it != optHashIDToGroup.end (); ++it)
    {
        if (it->second != NULL && (hash_id & it->first) == it->first)
        {
            vector<uint128_t>* a = ((Group*)it->second)->prevGroupsHashID();

            for (auto it2 = a->begin(); it2 != a->end(); ++it2)
            {
                if (((*it2) & hash_id) != (*it2))
                    prev_groups->push_back (*it2);
            }
        }
    }
        
    return prev_groups;
}

bool OptGroup::isLiveoutForGroup (uint128_t group, OptGroup* child)
{        
    for (auto const& next_group: child->nextGroups ())
    {
        if ((next_group->hashID () & group) != next_group->hashID ())
        {
            return true;
        }
    }
    
    if (child->nextGroups ().size () == 0)
        return true;
    
    return false;
}

void OptGroup::liveoutsForGroup (const uint128_t hash_id, 
                                const vector<Group*>& children, 
                                unordered_set<Group*, Group::GroupHasher>& liveouts)
{
    for (auto const& child: children)
    {
        bool is_liveout = false;
        
        for (auto const& next: child->nextGroups ())
        {
            if ((next->hashID() & hash_id) != next->hashID())
            {
                is_liveout = true;
                break;
            }                
        }
        
        if (child->nextGroups ().size () == 0)
            is_liveout = true;
            
        if (is_liveout)
        {
            liveouts.insert (child);
        }
    }
}

void OptGroup::liveinsForChildInGroup (uint128_t group, OptGroup* child, 
                                    unordered_set<Group*, Group::GroupHasher>& liveins)
{        
    for (auto const& prev_group: child->prevGroups ())
    {
        if ((prev_group->hashID () & group) != prev_group->hashID ())
        {
            liveins.insert (prev_group);
        }
    }
}