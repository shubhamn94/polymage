#include "../simple_pool_allocator.h"
#include "riffa_stream.h"
#include <chrono>
#include <cmath>
#include <malloc.h>
#include <pthread.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
void *send(void *);
void *recv(void *);
pthread_mutex_t mutex1 = PTHREAD_MUTEX_INITIALIZER;

std::chrono::time_point<std::chrono::high_resolution_clock> start, stop;

using namespace std;
using namespace std::chrono;

struct fpga_comm {

  riffa_stream fpga;
  send_fifo sBuffer;
  recv_fifo *rBuffer;
  fpga_comm(int siz) { rBuffer = new recv_fifo(siz); }
};
typedef struct fpga_comm fpga_comm;

extern "C" void sobel_fpga(int width, int height, void *img_void_arg,
                           void *sobel_void_arg) {
  uint16_t *imag_gray;
  imag_gray = (uint16_t *)(img_void_arg);
  float *sobel;
  sobel = (float *)(sobel_void_arg);
  uint16_t ch, input, i, j, k = 0, pack_dim[8], inp_pad = 0, out_pad = 0,
                            pad = 0, ROWS = 3;
  out_pad = 2 * ((4 - ((width / 2) % 4)) % 4);
  pthread_t thread1, thread2;
  int rc1, rc2;
  FILE *file;
  // Declaring pointers for packing data and dimensions
  uint32_t *pack_ptr, *packdim_ptr;

  // Declaring pointers for unpacking data
  uint16_t *unpack_ptr;

  // Declaring temporary array for storage of packed 32 bit data
  uint32_t size = ((width + out_pad) / 2) * (height - 2);
  const int SIZE = size;
  uint32_t *temp_result = new uint32_t[SIZE];

#pragma omp parallel for private(j) // schedule(static)
  for (i = 0; i < height; i++) {
#pragma ivdep
    for (j = 0; j < width; j++) {
      sobel[i * width + j] = imag_gray[i * width + j];
    }
  }

  pack_ptr = (uint32_t *)imag_gray;
  uint16_t *ptr = (uint16_t *)pack_ptr;

  printf("height = %d, width = %d ", height, width);
  pack_dim[0] = width;
  pack_dim[1] = height;
  pack_dim[2] = ROWS;
  pack_dim[4] = out_pad;
  packdim_ptr = (uint32_t *)pack_dim;

  fpga_comm *args = new fpga_comm(SIZE);

  inp_pad = (8 - (height * width) % 8) % 8;
  pack_dim[3] = inp_pad;

  //Padding and packing data such that send Buffer(sBuffer) data_len is divisible by 4 (128 bit packet)
  for (j = 5; j < (inp_pad + 8); j++)
    pack_dim[j] = pad;
  for (k = 0; k < ((inp_pad + 8) / 2); k++)
    args->sBuffer.push(packdim_ptr[k]);

  for (j = 0; j < height; j++) {
    for (k = 0; k < (width / 2); k++)
      args->sBuffer.push(pack_ptr[j * (width / 2) + k]);
  }

  //Creating seperate threads for send and receive so that both tasks can happen simultaneously
  if ((rc1 = pthread_create(&thread1, NULL, &send, (void *)args)))
    printf("Thread creation failed: %d\n", rc1);

  if ((rc2 = pthread_create(&thread2, NULL, &recv, (void *)args)))
    printf("Thread creation failed: %d\n", rc2);

  pthread_join(thread1, NULL);
  pthread_join(thread2, NULL);

  for (i = 0; i < (height - 2); i++) {
    for (j = 0; j < (width / 2); j++)
      temp_result[i * (width / 2) + j] = args->rBuffer->pop();
    for (j = 0; j < (out_pad / 2); j++)
      args->rBuffer->pop();
  }

  unpack_ptr = (uint16_t *)temp_result;

#pragma omp parallel for private(j)
  for (i = 1; i < (height - 1); i++)
    for (j = 0; j < width; j++)
#pragma ivdep
      sobel[i * width + j] = unpack_ptr[(i - 1) * width + j];

  delete args;
  delete[] temp_result;
  // Get duration. Substart timepoints to
  // get durarion. To cast it to proper unit
  // use duration cast method

  auto duration = duration_cast<milliseconds>(stop - start);
  cout << "Time taken for FPGA computation : " << duration.count() << " ms "
       << "\n";

#pragma omp parallel for private(j)
  for (i = 1; i < height - 1; i++) {
    for (j = 1; j < width - 1; j++) {
      if (sobel[i * width + j] <= 15)
        sobel[i * width + j] = 0;
      else
        sobel[i * width + j] = 255;
    }
  }
}

void *send(void *arguments) {
  fpga_comm *arg = (fpga_comm *)arguments;
  printf("Sending image");
  start = high_resolution_clock::now();
  arg->fpga << arg->sBuffer;
  pthread_exit(NULL);
  return NULL;
}

void *recv(void *arguments) {
  fpga_comm *arg = (fpga_comm *)arguments;
  arg->fpga >> *(arg->rBuffer);
  stop = high_resolution_clock::now();
  printf("receiving image");
  pthread_exit(NULL);
  return NULL;
}
