#include "../simple_pool_allocator.h"
#include <chrono>
#include <cmath>
#include <iostream>
#include <malloc.h>
#include <pthread.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

using namespace std;
using namespace std::chrono;

extern "C" void sobel_naive(int width, int height, void *img_void_arg,
                            void *sobel_void_arg) {
  uint8_t *imag_gray;
  imag_gray = (uint8_t *)(img_void_arg);
  float *sobel, curr;
  sobel = (float *)(sobel_void_arg);
  uint16_t i, j, k = 0;

#pragma omp parallel for private(j) // schedule(static)
  for (i = 0; i < height; i++) {
#pragma ivdep
    for (j = 0; j < width; j++) {
      sobel[i * width + j] = imag_gray[i * width + j];
    }
  }

  // Get starting timepoint
  auto start = high_resolution_clock::now();

  for (i = 1; i < (height - 1); i++) {
    for (j = 1; j < (width - 1); j++) {
      curr = imag_gray[(i - 1) * width + j - 1] +
             imag_gray[(i - 1) * width + j] +
             2 * imag_gray[(i - 1) * width + j + 1] -
             imag_gray[(i + 1) * width + j - 1] -
             2 * imag_gray[(i + 1) * width + j] -
             imag_gray[(i + 1) * width + j + 1];
      sobel[i * width + j] = (float)(curr * curr);
    }
  }

  for (i = 1; i < (height - 1); i++) {
    for (j = 1; j < (width - 1); j++) {
      curr = imag_gray[(i - 1) * width + j - 1] +
             2 * imag_gray[i * width + j - 1] +
             imag_gray[(i + 1) * width + j - 1] -
             imag_gray[(i - 1) * width + j + 1] -
             2 * imag_gray[i * width + j + 1] -
             imag_gray[(i + 1) * width + j + 1];
      sobel[i * width + j] += (float)(curr * curr);
      sobel[i * width + j] = sqrt((float)sobel[i * width + j]);
    }
  }
  // Get ending timepoint
  auto stop = high_resolution_clock::now();

  // Get duration. Substart timepoints to
  // get durarion. To cast it to proper unit
  // use duration cast method
  auto duration = duration_cast<milliseconds>(stop - start);
  cout << "Time taken for performing convolution in CPU: " << duration.count()
       << " ms "
       << "\n";

  //#pragma omp parallel for schedule(static)
  for (i = 1; i < height - 1; i++) {
    for (j = 1; j < width - 1; j++) {
      if (sobel[i * width + j] <= 15)
        sobel[i * width + j] = 0;
      else
        sobel[i * width + j] = 255;
    }
  }
}
