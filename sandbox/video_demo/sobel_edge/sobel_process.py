import ctypes
import numpy as np
from cv2 import *
import sys

sys.path.insert(0, "../")
from structs import *


def sobel_cv(frame, lib_func):
    sobel_kernel = 3
    gray = cvtColor(frame, COLOR_BGR2GRAY)
    gray = np.float32(gray) / 4.0
    sobel_x = cv2.Sobel(gray,cv2.CV_32F,1,0,ksize=sobel_kernel)
    sobel_y = cv2.Sobel(gray,cv2.CV_32F,0,1,ksize=sobel_kernel)
    sobel_abs = np.sqrt(sobel_x * sobel_x + sobel_y * sobel_y)
    sobel_scale = np.uint8(sobel_abs)
    '''file = open("sobel_cv_result","w+")
    for i in range(0,sobel_scale.shape[0]):
        for j in range(0,sobel_scale.shape[1]):
            file.write(str(sobel_scale[i][j]))
            file.write(" ")
        file.write("\n")

    file.close();'''
    sobel_scale = np.uint8(np.where(sobel_scale<=15, 0, 255))
    return sobel_scale

def sobel_fpga(frame, lib_func):
    rows = frame.shape[0]
    cols = frame.shape[1]
    gray = cvtColor(frame, COLOR_BGR2GRAY)
    gray = np.int16(gray) / 4
    res = np.empty((rows, cols), np.float32)
    lib_func(ctypes.c_int(cols), \
             ctypes.c_int(rows), \
             ctypes.c_void_p(gray.ctypes.data), \
             ctypes.c_void_p(res.ctypes.data))
    return res

def sobel_naive(frame, lib_func):
    rows = frame.shape[0]
    cols = frame.shape[1]
    gray = cvtColor(frame, COLOR_BGR2GRAY)
    gray = np.uint8(gray) / 4
    res = np.empty((rows, cols), np.float32)
    lib_func(ctypes.c_int(cols), \
             ctypes.c_int(rows), \
             ctypes.c_void_p(gray.ctypes.data), \
             ctypes.c_void_p(res.ctypes.data))
    return res

def add_sobel_app(app_id):
    # 1. modes
    modes = [ModeType.CV2, ModeType.S_NAIVE, ModeType.S_FPGA] 
    # 2. modes that need shared library
    lib_modes = [ModeType.S_NAIVE, ModeType.S_FPGA]

    # 3. set python functions from frame_process.py
    app_func_map = {}
    app_func_map[ModeType.CV2] = sobel_cv
    app_func_map[ModeType.S_NAIVE] = sobel_naive
    app_func_map[ModeType.S_FPGA] = sobel_fpga

    # 4. create an App object
    app_dir = os.path.dirname(os.path.realpath(__file__))
    app = App(app_id, app_dir, modes, lib_modes, app_func_map)

    return app

